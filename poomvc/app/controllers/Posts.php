<?php

use Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{

    private $postModel;
    private $userModel;

    public function __construct()
    {
        //Protege la vista posts mediante la función isLoggedIn
        // Para ello comprueba en el constructor de Posts, con la función isLoggedIn, si un usuario no está  logueado
        if (!isLoggedIn()) {
            urlRedirect('/users/login');
        }

        $this->postModel = $this->model('Post');
        $this->userModel = $this->model('User');
    }

    // método index que cargue la vista index de post pasándole el array $data.

    public function index()
    {

        $posts = $this->postModel->getPosts();

        $data = [
            'titulo' => 'Index de posts',
            'posts' => $posts
        ];

        return $this->view('posts/index', $data);
    }

    
}
