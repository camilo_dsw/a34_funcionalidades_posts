<?php require_once APPROOT . '/views/partials/header.php'; ?>
<div class="container">

    <div class="row mb-3">
        <div class="flashes">
            <?= (string) flash() ?>
        </div>
        <div class="col-md-6">
            <h1>Publicaciones</h1>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary pull-right" href="" role="button">
                <i class="fas fa-pencil-alt"></i> Crear publicación
            </a>
        </div>
        <?php foreach ($data['posts'] as $post) : ?>
            <div class="card">
                <div class="card-header">
                    Creado por <?= $post->name ?> este <?= $post->postCreatedAt ?>

                </div>
                <div class="card-body">
                    <h5 class="card-title"><?= $post->title ?></h5>
                    <p class="card-text"><?= $post->body ?></p>
                    <a href="<?= URLROOT . "/posts/show/$post->postId" ?>" class="btn btn-primary">Más</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php require_once APPROOT . '/views/partials/footer.php'; ?>