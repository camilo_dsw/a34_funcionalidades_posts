<?php

class User
{
    
    private $db;

    public function __construct()
    {
        // instanciamos  a Database
        $this->db = new Database();
    }

    // método que nos permite la busqueda en la tabla users del usuario que tiene un email en concreto

    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * from users WHERE email = :email');
        $this->db->bind(':email', $email);
        $row = $this->db->obtnerUsuario('User'); 

        if (empty($row)) {
            return false;
        } else {
           
            return true;
        }
        
        
    }

    public function register($data)
    {
        $this->db->query("INSERT into users(name, email, password) values(:name, :email, :password)");
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);
        $result = $this->db->execute();


        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    public function login($email, $password)
    {

        $this->db->query("SELECT * from users where email = :email");
        $this->db->bind(':email', $email);

        $row = $this->db->obtnerUsuario('User');

           
        if (password_verify($password, $row->password)) {
            return $row;
        } else {
            return false;
        }
        
    }
   

    
}
